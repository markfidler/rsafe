*** Settings ***
Documentation     Acceptance Tests
Library           SeleniumLibrary
Library           OperatingSystem

*** Variables ***
${MAIN URL}=        %{URL}
${BROWSER}=         %{BROWSER}

*** Test Cases ***
Main Tests
    Open Browser To Main Page

*** Keywords ***
Open Browser To Main Page
    Open Browser            ${MAIN URL}    ${BROWSER}
    Title Should Be         Hello World Example
    Page Should Contain     Hello World

