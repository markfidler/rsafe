# rSafe Auto Build

## Introduction

This repository contains the code required to quickly & consistently build (and test) a simple website. It uses vagrant and performs all the tasks it requires on a "vagrant box", meaning that all users of this repository should have a consistent experience.

## My Setup

I am using "MS Windows 10 Home" with the following installed:

* VirtualBox 5.2.22
* Vagrant 2.2.2

NB My example uses Vagrant box "bento/centos-7.4" and I tested this with the latest version available at the time, v201803.24.0

## Quick Start 

* First, clone this repo.
* Then bring up the vagrant server:

```
vagrant up
```

Note that this may take a while

* Now open your favourite browser and navigate directly to the VM using [http://10.100.198.200:18080/] or you can navigate to [http://localhost:8080/] (which will have been forwarded for your convenience).

## Tests

After successful execution of the `vagrant up` command, there should be two folders created in `./tests/reports` folder, one called "chrome" and another called "firefox" which represent the results of the acceptance tests (specified in `./tests/acceptance.robot`) in both of those browsers. In each folder, you should see a test report and a test log in HTML format, which you can open in a browser. As you can see from the `./tests/acceptance.robot` file, the robot framework is in a language that is quite high level and could possibly be understood to a Business Analyst or perhaps even an end client. Under the hood however, it is using Selenium, a popular framework for browser testing.

## Nuances & comments

* I haven't used the Ansible provisioner per se, I have used an in-line shell command to run ansible "local" to the vagrant box. This was historically preferable when using a Windows host.
* I generally would prefer to use packer to create images of each flavour of VM I need, rather than use foundation images (i.e. where a vanilla Centos VM is used and an ansible playbook is than executed for every instance of that host I spin up). This saves time during 'instance provisioning' and pushes the execution of the playbook left up the CI/CD pipeline, allowing you to test your images post playbook at build time.
* I would typically use a CI/CD orchestration tool (e.g. jenkins, Team City, etc.) to trigger tasks such as app builds, rather than Ansible. This repo is mixing a couple of use cases together. i.e. we have one repo which builds infrastructure (a VM containing Docker, etc.) and also the application & tests. Generally I would expect more separation between the two.